<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Mini Tabs - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                3. Mini Tabs
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Mini Tabs</li>
                    </ol>
                </nav>
            </h5>

            <div class="content-ati">
                <p style="margin-top: 12px;">Lorem, ipsum dolor sit amet <b>consectetur</b> adipisicing elit. Quod, sit
                    corrupti. <b>Suscipit</b> vero <b>molestias</b> nesciunt facere sequi accusamus cum aut, magni
                    praesentium dolorum assumenda a quasi aliquid reprehenderit molestiae mollitia.</pb>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="group-item-ati">
                                <h6><b>TITLE DEMO-ATI</b></h6>
                                <div class="group-btn-3">
                                    <a href="#" class="btn-page-3 btn-color-1">Choice ATI #1</a>
                                    <a href="#" class="btn-page-3 btn-color-2">Choice ATI #2</a>
                                    <a href="#" class="btn-page-3 btn-color-3">Choice ATI #3</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="item-page-3" id="itembox-1">
                                <li> <b>Lorem ipsum</b> dolor sit amet consectetur adipisicing elit. Maxime facere nemo
                                    error reprehenderit architecto. Eaque dicta at dolorum totam in aliquam, deserunt
                                    laborum nihil. Dolorum qui totam perspiciatis ab ut!</li>
                            </div>
                            <div class="item-page-3" id="itembox-2">
                                <li><b>Lorem ipsum </b>dolor sit amet consectetur adipisicing elit. Maxime facere nemo
                                    error reprehenderit architecto. Eaque dicta at dolorum totam in aliquam, deserunt
                                    laborum nihil. Dolorum qui totam perspiciatis ab ut!</li>
                            </div>
                            <div class="item-page-3" id="itembox-3">
                                <li><b>Lorem ipsum</b> dolor sit amet consectetur adipisicing elit. Maxime facere nemo
                                    error reprehenderit architecto. Eaque dicta at dolorum totam in aliquam, deserunt
                                    laborum nihil. Dolorum qui totam perspiciatis ab ut!</li>
                            </div>
                        </div>
                    </div>
            </div>





        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script>
        $(document).ready(function () {
            $(".btn-color-1").click(function () {
                $(" #itembox-1").toggleClass("item-show");
            });
            $(".btn-color-2").click(function () {
                $("#itembox-2").toggleClass("item-show");
            });
            $(".btn-color-3").click(function () {
                $("#itembox-3").toggleClass("item-show");
            });

        });
    </script>

</body>

</html>