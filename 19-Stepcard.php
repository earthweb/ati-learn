<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Step card - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
        <link href="themes/template/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                19. Step card
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Step card</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab" style="margin-top:25px;">
                                <div class="card more-tab" >
                                    <div class="card-body">
                                        <h5 class="card-title"><b>Step1:</b></h5>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                        up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab fade-in ">
                                <div class="card more-tab">
                                    <div class="card-body">
                                        <h5 class="card-title"><b>Step2:</b></h5>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                        up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab fade-in ">
                                <div class="card more-tab">
                                    <div class="card-body">
                                        <h5 class="card-title"><b>Step3:</b></h5>
                                        <p class="card-text">Some quick example text to build on the card title and make
                                        up the bulk of the card's content.</p>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 25px">
                                <div style="text-align: center">
                                    <!--<button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>-->
                                    <button type="button" id="nextBtn" class="btn btn-success"
                                    style="height: 38px;width: 75px;" onclick="nextPrev(1)"><i
                                    class="fas fa-angle-down"></i></button>
                                </div>
                            </div>
                            <!-- Circles which indicates the steps of the form: -->
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'themes/template/include/footer.php'; ?>
        </div>
        <?php include 'themes/template/include/javascript.php'; ?>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>


<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        // if (n == 0) {
        //     document.getElementById("prevBtn").style.display = "none";
        // } else {
        //     document.getElementById("prevBtn").style.display = "inline";
        // }
        if (n == (x.length - 1)) {

            document.getElementById('nextBtn').style.visibility = 'hidden';
        } else {
            document.getElementById("nextBtn").innerHTML = "<i class=\"fas fa-angle-down\"></i>";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:

        // Hide the current tab:
        x[currentTab].style.display = "block";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }


    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>
    </body>
</html>