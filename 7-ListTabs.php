themes/template<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


    <title>List Tabs - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                7. List Tabs
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">List Tabs</li>
                    </ol>
                </nav>
            </h5>


            <div class="content-ati">
                <div class="row">
                    <div class="col-md-4">
                        <a href="#">
                            <div class="tab-ati-7 ati-7-1" data-aos="fade-right" data-aos-easing="ease-in-sine">Lorem,
                                ipsum dolor sit amet consectetur adipisicing elit. Architecto,</div>
                        </a>
                        <a href="#">
                            <div class="tab-ati-7 ati-7-2" data-aos="fade-right" data-aos-delay="300"
                                data-aos-easing="ease-in-sine">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Architecto,</div>
                        </a>
                        <a href="#">
                            <div class="tab-ati-7 ati-7-3" data-aos="fade-right" data-aos-delay="500"
                                data-aos-easing="ease-in-sine">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Architecto,</div>
                        </a>
                        <a href="#">
                            <div class="tab-ati-7 ati-7-4" data-aos="fade-right" data-aos-delay="700"
                                data-aos-easing="ease-in-sine">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Architecto,</div>
                        </a>
                    </div>

                    <div class="col-md-12">
                        <div class="content-7ati 7v1">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h5><span class="c-primary">1.</span> Lorem ipsum dolor sit amet consectetur adipisicing
                                elit.</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                repellendus voluptatem eum perferendis nihil. Accusantium laboriosam similique
                                repellendus sed, commodi ipsam accusamus eveniet! A tempore dolorum exercitationem?</p>
                            <ul>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                            </ul>
                            <div class="text-center"><img src="themes/template/img/7-ati-1.png" alt="" style="padding-top:15px;"></div>
                        </div>
                        <div class="content-7ati 7v2">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h5><span class="c-primary">2.</span> Lorem ipsum dolor sit amet consectetur adipisicing
                                elit.</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                repellendus voluptatem eum perferendis nihil. Accusantium laboriosam similique
                                repellendus sed, commodi ipsam accusamus eveniet! A tempore dolorum exercitationem?</p>
                            <ul>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                            </ul>
                            <div class="text-center"><img src="themes/template/img/7-ati-2.png" alt="" style="padding-top:15px;"></div>
                        </div>

                        <div class="content-7ati 7v3">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h5><span class="c-primary">3.</span> Lorem ipsum dolor sit amet consectetur adipisicing
                                elit.</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                repellendus voluptatem eum perferendis nihil. Accusantium laboriosam similique
                                repellendus sed, commodi ipsam accusamus eveniet! A tempore dolorum exercitationem?</p>
                            <ul>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                            </ul>
                            <div class="text-center"><img src="themes/template/img/7-ati-3.png" alt="" style="padding-top:15px;"></div>
                        </div>

                        <div class="content-7ati 7v4">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h5><span class="c-primary">4.</span> Lorem ipsum dolor sit amet consectetur adipisicing
                                elit.</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                repellendus voluptatem eum perferendis nihil. Accusantium laboriosam similique
                                repellendus sed, commodi ipsam accusamus eveniet! A tempore dolorum exercitationem?</p>
                            <ul>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum, beatae nobis eius
                                    repellendus voluptatem </li>
                            </ul>
                            <div class="text-center"><img src="themes/template/img/7-ati-4.png" alt="" style="padding-top:15px;"></div>
                        </div>
                    </div>

                </div>
            </div>




        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <script>
        $(document).ready(function () {
            $(".ati-7-1").click(function () {
                $(".tab-ati-7").hide("slow");
                $(".7v1").show("slow");
            });
            $(".close-view").click(function () {
                $(".7v1").hide("slow");
                $(".tab-ati-7").show("slow");
            });
            $(".ati-7-2").click(function () {
                $(".tab-ati-7").hide("slow");
                $(".7v2").show("slow");
            });
            $(".close-view").click(function () {
                $(".7v2").hide("slow");
                $(".tab-ati-7").show("slow");
            });
            $(".ati-7-3").click(function () {
                $(".tab-ati-7").hide("slow");
                $(".7v3").show("slow");
            });
            $(".close-view").click(function () {
                $(".7v3").hide("slow");
                $(".tab-ati-7").show("slow");
            });
            $(".ati-7-4").click(function () {
                $(".tab-ati-7").hide("slow");
                $(".7v4").show("slow");
            });
            $(".close-view").click(function () {
                $(".7v4").hide("slow");
                $(".tab-ati-7").show("slow");
            });
        });
    </script>

</body>

</html>