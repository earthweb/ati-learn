<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Multi Toggle - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                15. Multi Toggle
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Multi Toggle</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="toggle-multi">
                                <div class="card-toggle-ati">
                                    
                                    <div class="collapse content-toggle primary" id="card-ati-1">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, earum eius deserunt alias atque asperiores molestiae vel tenetur expedita.</p>
                                    </div>
                                    <a class="head-toggle page-15" data-toggle="collapse" href="#card-ati-1" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        01
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="toggle-multi">
                                <div class="card-toggle-ati">
                                    
                                    <div class="collapse content-toggle primary" id="card-ati-2">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, earum eius deserunt alias atque asperiores molestiae vel tenetur expedita.</p>
                                    </div>
                                    <a class="head-toggle page-15" data-toggle="collapse" href="#card-ati-2" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        02
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="toggle-multi">
                                <div class="card-toggle-ati">
                                    
                                    <div class="collapse content-toggle primary" id="card-ati-3">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, earum eius deserunt alias atque asperiores molestiae vel tenetur expedita.</p>
                                    </div>
                                    <a class="head-toggle page-15" data-toggle="collapse" href="#card-ati-3" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        03
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="toggle-multi">
                                <div class="card-toggle-ati">
                                    
                                    <div class="collapse content-toggle primary" id="card-ati-4">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, earum eius deserunt alias atque asperiores molestiae vel tenetur expedita.</p>
                                    </div>
                                    <a class="head-toggle page-15" data-toggle="collapse" href="#card-ati-4" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        04
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="toggle-multi">
                                <div class="card-toggle-ati">
                                    
                                    <div class="collapse content-toggle primary" id="card-ati-5">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, earum eius deserunt alias atque asperiores molestiae vel tenetur expedita.</p>
                                    </div>
                                    <a class="head-toggle page-15" data-toggle="collapse" href="#card-ati-5" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        05
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="toggle-multi">
                                <div class="card-toggle-ati">
                                    
                                    <div class="collapse content-toggle primary" id="card-ati-6">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, earum eius deserunt alias atque asperiores molestiae vel tenetur expedita.</p>
                                    </div>
                                    <a class="head-toggle page-15" data-toggle="collapse" href="#card-ati-6" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        06
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <?php include 'themes/template/include/footer.php'; ?>
    </div>
    <?php include 'themes/template/include/javascript.php'; ?>
     <script>
    $(document).ready(function(){
      $( ".step-btn-1" ).click(function() {
      $(" .step-box-1").show( 'slow');
      $(" .step-box-2").hide( 'slow');
      $(" .step-box-3").hide( 'slow');
      $(" .step-box-4").hide( 'slow');
      $(".step-btn-1").addClass('current');
      });
      $( ".step-btn-2" ).click(function() {
      $(" .step-box-2").show( 'slow');
      $(" .step-box-1").hide( 'slow');
      $(" .step-box-3").hide( 'slow');
      $(" .step-box-4").hide( 'slow');
      $(".step-btn-2").addClass('current');
    //   $(".step-btn-1").removeClass('current');
    //   $(".step-btn-3").removeClass('current');
    //   $(".step-btn-4").removeClass('current');
      });
      $( ".step-btn-3" ).click(function() {
      $(" .step-box-3").show( 'slow');
      $(" .step-box-2").hide( 'slow');
      $(" .step-box-1").hide( 'slow');
      $(" .step-box-4").hide( 'slow');
      $(".step-btn-3").addClass('current');
      });
      $( ".step-btn-4" ).click(function() {
      $(" .step-box-4").show( 'slow');
      $(" .step-box-1").hide( 'slow');
      $(" .step-box-2").hide( 'slow');
      $(" .step-box-3").hide( 'slow');
      $(".step-btn-4").addClass('current');
      });
    });
    </script>

</body>
</html>