<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Toggle Box - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                1. Toggle Box
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>

            <div class="content-ati">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card-toggle-ati">
                            <a class="head-toggle danger" data-toggle="collapse" href="#card-ati-1" role="button"
                                aria-expanded="false" aria-controls="collapseExample">
                                Thinking
                                <span class="float-right"><i class="far fa-check-circle"></i></span>
                            </a>
                            <div class="collapse content-toggle danger" id="card-ati-1">

                                <img src="themes/template/img/icon-1.png" alt="">
                                <p style="padding:0.2em;"> Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-toggle-ati">
                            <a class="head-toggle warning" data-toggle="collapse" href="#card-ati-2" role="button"
                                aria-expanded="false" aria-controls="collapseExample">
                                Barainstorm
                                <span class="float-right"><i class="far fa-check-circle"></i></span>
                            </a>
                            <div class="collapse content-toggle warning" id="card-ati-2">

                                <img src="themes/template/img/icon-2.png" alt="">
                                <p style="padding:0.2em;"> Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-toggle-ati">
                            <a class="head-toggle success" data-toggle="collapse" href="#card-ati-3" role="button"
                                aria-expanded="false" aria-controls="collapseExample">
                                Development
                                <span class="float-right"><i class="far fa-check-circle"></i></span>
                            </a>
                            <div class="collapse content-toggle success" id="card-ati-3">

                                <img src="themes/template/img/icon-3.png" alt="">
                                <p style="padding:0.2em;"> Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-content">
                <ul>
                    <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas eligendi soluta nesciunt enim
                        repudiandae dolor ullam dolores incidunt aliquam ipsa molestias, unde nam impedit ratione
                        aspernatur voluptatem nulla minima odio!</li>
                    <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores accusantium cupiditate
                        temporibus quo voluptatibus iste accusamus reiciendis incidunt saepe sequi culpa tempora ab
                        deleniti inventore quis aliquam rerum, magnam quas!</li>
                </ul>
            </div>
        </div>





    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>


</body>

</html>