<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Number Toggle - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                12. Number Toggle
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Number Toggle</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <div class=""><p>Click on each <b>Number</b> to Learn more.</p></div>
                            <div class="number-box">
                                <a href="#" class="point-1">1</a>
                                <a href="#" class="point-2">2</a>
                                <a href="#" class="point-3">3</a>
                                <a href="#" class="point-4">4</a>
                                <p class="content-12-1"><b>1.</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, veritatis accusantium distinctio libero quos tenetur, quia officia, eligendi at vitae labore illum incidunt recusandae velit laudantium expedita neque tempora sequi.</p>
                                <p class="content-12-2"><b>2.</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, veritatis accusantium distinctio libero quos tenetur, quia officia, eligendi at vitae labore illum incidunt recusandae velit laudantium expedita neque tempora sequi.</p>
                                <p class="content-12-3"><b>3.</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, veritatis accusantium distinctio libero quos tenetur, quia officia, eligendi at vitae labore illum incidunt recusandae velit laudantium expedita neque tempora sequi.</p>
                                <p class="content-12-4"><b>4.</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, veritatis accusantium distinctio libero quos tenetur, quia officia, eligendi at vitae labore illum incidunt recusandae velit laudantium expedita neque tempora sequi.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'themes/template/include/footer.php'; ?>
        </div>
        <?php include 'themes/template/include/javascript.php'; ?>
<script>
    AOS.init();
</script>
        <script>
        $(document).ready(function(){
        $( ".point-1" ).click(function() {
        $(".content-12-1").show("slow");
        });
        $( ".point-2" ).click(function() {
        $(".content-12-2").show("slow");
        });
        $( ".point-3" ).click(function() {
        $(".content-12-3").show("slow");
        });
        $( ".point-4" ).click(function() {
        $(".content-12-4").show("slow");
        });
        
        });</script>
    </body>
</html>