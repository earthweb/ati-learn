<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <title>List Box Tabs - ATI </title>
    <?php include 'themes/template/include/css.php'; ?>
  </head>
  <body>
    <div class="body ia-main">
      <?php include 'themes/template/include/header.php'; ?>
      <div class="frame-ati">
        <h5 class="title-h5">
        13. List Box Tabs
        <nav aria-label="breadcrumb">
          <div class="clearfix"></div>
          <ol class="breadcrumb bc-ati">
            <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">List Box Tabs</li>
          </ol>
        </nav>
        </h5>
        <div class="content-ati">
          <br>
          <p>Lorem, ipsum dolor sit amet <b>consectetur</b> adipisicing elit. Quod, sit corrupti. <b>Suscipit</b> vero <b>molestias</b> nesciunt facere sequi</p>
          <div class="row">
            <div class="col-md-2">
              <div class="group-item-ati">
                <div class="group-btn-3">
                  <a href="#" class="btn-nume-13 btn-num-1">1</a>
                  <a href="#" class="btn-nume-13 btn-num-2">2</a>
                  <a href="#" class="btn-nume-13 btn-num-3">3</a>
                  <a href="#" class="btn-nume-13 btn-num-4">4</a>
                </div>
              </div>
            </div>
            <div class="col-md-10">
              <div class="tab-number-13 nume-1">
                <p>
                  <b>1.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                </p>
                <div class="text-center"><img src="themes/template/img/icon/1.png" alt="" style="padding-top:20px;"></div>
              </div>
              <div class="tab-number-13 nume-2">
                <p>
                  <b>2.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                </p>
                <div class="text-center"><img src="themes/template/img/icon/2.png" alt="" style="padding-top:20px;"></div>
              </div>
              <div class="tab-number-13 nume-3">
                <p>
                  <b>3.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                </p>
                <div class="text-center"><img src="themes/template/img/icon/3.png" alt="" style="padding-top:20px;"></div>
              </div>
              <div class="tab-number-13 nume-4">
                <p>
                  <b>4.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                </p>
                <div class="text-center"><img src="themes/template/img/icon/4.png" alt="" style="padding-top:20px;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include 'themes/template/include/footer.php'; ?>
  </div>
  <?php include 'themes/template/include/javascript.php'; ?>
  <script>
    $(document).ready(function(){
      $( ".btn-num-1" ).click(function() {
      $(" .nume-1").show( 'slow');
      $(" .nume-2").hide( 'slow');
      $(" .nume-3").hide( 'slow');
      $(" .nume-4").hide( 'slow');
    });
      $( ".btn-num-2" ).click(function() {
        $(" .nume-2").show( 'slow');
        $(" .nume-1").hide( 'slow');
        $(" .nume-3").hide( 'slow');
        $(" .nume-4").hide( 'slow');
    });
      $( ".btn-num-3" ).click(function() {
        $(" .nume-3").show( 'slow');
        $(" .nume-1").hide( 'slow');
        $(" .nume-2").hide( 'slow');
        $(" .nume-4").hide( 'slow');
    });
      $( ".btn-num-4" ).click(function() {
        $(" .nume-4").show( 'slow');
        $(" .nume-1").hide( 'slow');
        $(" .nume-2").hide( 'slow');
        $(" .nume-3").hide( 'slow');
    });
  });
  </script>
</body>
</html>