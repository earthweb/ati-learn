<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Circle Tabs - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                16. Circle Tabs
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Circle Tabs</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="col"><br><p>Click <b>Images</b> to Learn more</p></div>
                        <div class="col-md-12">
                            <div class="progress-box" align="center">
                                <img src="themes/template/img/progress-1.png" class="btn-img-1" alt="" >
                                <img src="themes/template/img/progress-2.png" class="btn-img-2" alt="" >
                                <img src="themes/template/img/progress-3.png" class="btn-img-3" alt="" >
                                <img src="themes/template/img/progress-4.png" class="btn-img-4" alt="" >
                                <img src="themes/template/img/progress-5.png" class="btn-img-5" alt="" >
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="box-img-text img-text-1">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!
                                <div class="text-center"><img src="themes/template/img/Asset 1.png" style="margin-top: 1.2em"  alt=""></div>
                            </div>
                            <div class="box-img-text img-text-2">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!
                                <div class="text-center"><img src="themes/template/img/Asset 2.png" style="margin-top: 1.2em"  alt=""></div>
                            </div>
                            <div class="box-img-text img-text-3">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!
                                <div class="text-center"><img src="themes/template/img/Asset 3.png" style="margin-top: 1.2em"  alt=""></div>
                            </div>
                            <div class="box-img-text img-text-4">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!
                                <div class="text-center"><img src="themes/template/img/Asset 4.png" style="margin-top: 1.2em"  alt=""></div>
                            </div>
                            <div class="box-img-text img-text-5">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!
                                <div class="text-center"><img src="themes/template/img/Asset 5.png" style="margin-top: 1.2em"  alt=""></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <?php include 'themes/template/include/footer.php'; ?>
    </div>
    <?php include 'themes/template/include/javascript.php'; ?>
    <script>
    $(document).ready(function(){
    $( ".btn-img-1" ).click(function() {
    $(" .img-text-1").show( 'slow');
    $(" .img-text-2").hide( 'slow');
    $(" .img-text-3").hide( 'slow');
    $(" .img-text-4").hide( 'slow');
    $(" .img-text-5").hide( 'slow');
    });
    $( ".btn-img-2" ).click(function() {
    $(" .img-text-2").show( 'slow');
    $(" .img-text-1").hide( 'slow');
    $(" .img-text-3").hide( 'slow');
    $(" .img-text-4").hide( 'slow');
    $(" .img-text-5").hide( 'slow');
    
    });
    $( ".btn-img-3" ).click(function() {
    $(" .img-text-3").show( 'slow');
    $(" .img-text-1").hide( 'slow');
    $(" .img-text-2").hide( 'slow');
    $(" .img-text-4").hide( 'slow');
    $(" .img-text-5").hide( 'slow');
    });
    $( ".btn-img-4" ).click(function() {
    $(" .img-text-4").show( 'slow');
    $(" .img-text-1").hide( 'slow');
    $(" .img-text-2").hide( 'slow');
    $(" .img-text-3").hide( 'slow');
    $(" .img-text-5").hide( 'slow');
    });
    $( ".btn-img-5" ).click(function() {
    $(" .img-text-5").show( 'slow');
    $(" .img-text-1").hide( 'slow');
    $(" .img-text-2").hide( 'slow');
    $(" .img-text-3").hide( 'slow');
    $(" .img-text-4").hide( 'slow');
    });
    });
    </script>
</body>
</html>