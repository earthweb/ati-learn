<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png"/>

    <title>Interactive e-Learning - ATI </title>

     <?php include 'themes/template/include/css.php';?> 

</head>

<body>

    <div class="body ia-main">
        
        <?php include 'themes/template/include/header.php';?>

        <?php include 'themes/template/include/main.php';?>

        <?php include 'themes/template/include/footer.php';?>

    </div>

        <?php include 'themes/template/include/javascript.php';?> 


</body>

</html>