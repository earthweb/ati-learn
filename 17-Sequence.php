<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Sequence - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                17. Sequence
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sequence</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="list-sort">
                                <h6 style="margin-bottom:2em;">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h6>
                                <div class="box-sort">
                                    <span class="numsort-1"></span>
                                    <span class="numsort-2"></span>
                                    <span class="numsort-3"></span>
                                    <span class="numsort-4"></span>
                                    <span class="numsort-5"></span>
                                </div>
                                <ul id="sortable" class="box-listsort">
                                    <li class="ui-state-default sequence-list"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item A
                                    <span class="float-right"><i class="fa fa-check" aria-hidden="true"></i></span>
                                </li>
                                <li class="ui-state-default sequence-list"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item B
                                <span class="float-right"><i class="fa fa-check" aria-hidden="true"></i>
                                </li>
                                <li class="ui-state-default sequence-list"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item C
                                <span class="float-right"><i class="fa fa-check" aria-hidden="true"></i>
                                </li>
                                <li class="ui-state-default sequence-list"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item D
                                <span class="float-right"><i class="fa fa-check" aria-hidden="true"></i>
                                </li>
                                <li class="ui-state-default sequence-list"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item E
                                <span class="float-right"><i class="fa fa-check" aria-hidden="true"></i>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-list-img">
                            <img src="themes/template/img/list-sq.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="btngroup-sort">
                    <a href="#" class="submit-list">Submit</a>
                    <a href="#" class="answer-list">Show Answer</a>
                    <a href="#" class="reset-list"><i class="fas fa-redo"></i> </a>
                </div>
            </div>
        </div>
    </div>
    <?php include 'themes/template/include/footer.php'; ?>
</div>
<?php include 'themes/template/include/javascript.php'; ?>
    <script src="themes/template/js/jquery-3.3.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
        $( function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
        } );
    </script>
     <script>
        $(document).ready(function(){
            $( ".submit-list" ).click(function() {
            swal("Good job!", "You clicked the button!", "success");
            $(".submit-list").hide();
            $(".answer-list").show();
            $(".reset-list").show();
         });
            $(".answer-list").click(function(){
                $(".fa-check").show();
            });
            $(".reset-list").click(function(){
                location.reload();
            });
       });
       </script>

</body>
</html>