<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Modal Circle - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                5. Modal Circle
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Modal Circle</li>
                    </ol>
                </nav>
            </h5>



            <div class="content-ati">
                <div class="row">
                    <div class="col-md-3 ">
                        <div class="user-ati user-sh" id="user-1">
                            <a href="#"><img src="themes/template/img/people/1.png" alt="">
                                <p>ATI Profile</p>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-3 ">
                        <div class="user-ati user-sh" id="user-2">
                            <a href="#"><img src="themes/template/img/people/2.png" alt="">
                                <p>ATI Profile</p>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-3 ">
                        <div class="user-ati user-sh" id="user-3">
                            <a href="#"><img src="themes/template/img/people/3.png" alt="">
                                <p>ATI Profile</p>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-3 ">
                        <div class="user-ati user-sh" id="user-4">
                            <a href="#"><img src="themes/template/img/people/4.png" alt="">
                                <p>ATI Profile</p>
                            </a>
                        </div>
                    </div>
                    <!-- set1 -->
                    <div class="col-md-3">
                        <div class="user-atihide viewuser-1">
                            <img src="themes/template/img/people/1-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="allviewuser textviewuser-1">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h6><b>ATI Profile 1</b></h6>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, pariatur. Non, quae
                                beatae atque consequuntur repudiandae rem velit veniam, facilis repellat sed
                                exercitationem accusantium, tenetur iste impedit. Nobis, molestiae qui.
                            </p>
                            <h6><b>word :</b></h6>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor eveniet earum libero alias
                                temporibus fugiat explicabo, eius, aliquid ipsa ducimus porro incidunt. Quod error
                                doloribus excepturi. Et a exercitationem sapiente.</p>
                        </div>
                    </div>
                    <!-- set2 -->
                    <div class="col-md-3">
                        <div class="user-atihide viewuser-2">
                            <img src="themes/template/img/people/2-2.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="allviewuser textviewuser-2">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h6><b>ATI Profile 2</b></h6>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, pariatur. Non, quae
                                beatae atque consequuntur repudiandae rem velit veniam, facilis repellat sed
                                exercitationem accusantium, tenetur iste impedit. Nobis, molestiae qui.
                            </p>
                            <h6><b>word :</b></h6>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor eveniet earum libero alias
                                temporibus fugiat explicabo, eius, aliquid ipsa ducimus porro incidunt. Quod error
                                doloribus excepturi. Et a exercitationem sapiente.</p>
                        </div>
                    </div>
                    <!-- set3 -->
                    <div class="col-md-3">
                        <div class="user-atihide viewuser-3">
                            <img src="themes/template/img/people/3-3.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="allviewuser textviewuser-3">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h6><b>ATI Profile 3</b></h6>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, pariatur. Non, quae
                                beatae atque consequuntur repudiandae rem velit veniam, facilis repellat sed
                                exercitationem accusantium, tenetur iste impedit. Nobis, molestiae qui.
                            </p>
                            <h6><b>word :</b></h6>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor eveniet earum libero alias
                                temporibus fugiat explicabo, eius, aliquid ipsa ducimus porro incidunt. Quod error
                                doloribus excepturi. Et a exercitationem sapiente.</p>
                        </div>
                    </div>
                    <!-- set4 -->
                    <div class="col-md-3">
                        <div class="user-atihide viewuser-4">
                            <img src="themes/template/img/people/4-4.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="allviewuser textviewuser-4">
                            <a href="#"><i class="fas fa-times-circle close-view"></i></a>
                            <h6><b>ATI Profile 4</b></h6>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, pariatur. Non, quae
                                beatae atque consequuntur repudiandae rem velit veniam, facilis repellat sed
                                exercitationem accusantium, tenetur iste impedit. Nobis, molestiae qui.
                            </p>
                            <h6><b>word :</b></h6>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor eveniet earum libero alias
                                temporibus fugiat explicabo, eius, aliquid ipsa ducimus porro incidunt. Quod error
                                doloribus excepturi. Et a exercitationem sapiente.</p>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger ati-alert" role="alert">
                    Click on <b>Image</b> to learn more
                </div>
            </div>



        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script>
        $(document).ready(function () {
            $("#user-1").click(function () {
                $(".user-sh").hide("slow");
                $(".ati-alert").hide("slow");
                $(".textviewuser-1").show('slow');
                $(".viewuser-1").show('slow');
            });
            $("#user-2").click(function () {
                $(".user-sh").hide("slow");
                $(".ati-alert").hide("slow");
                $(".textviewuser-2").show('slow');
                $(".viewuser-2").show('slow');
            });
            $("#user-3").click(function () {
                $(".user-sh").hide("slow");
                $(".ati-alert").hide("slow");
                $(".textviewuser-3").show('slow');
                $(".viewuser-3").show('slow');
            });
            $("#user-4").click(function () {
                $(".user-sh").hide("slow");
                $(".ati-alert").hide("slow");
                $(".textviewuser-4").show('slow');
                $(".viewuser-4").show('slow');
            });
            $(".close-view").click(function () {
                $(".user-sh").show("slow");
                $(".user-atihide").hide("slow");
                $(".allviewuser").hide("slow");
                $(".ati-alert").show("slow");
            });

        });
    </script>

</body>

</html>