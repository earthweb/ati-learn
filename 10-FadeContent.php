<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title>Fade Content - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

        <h5 class="title-h5">
                10. Fade Content
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Fade Content</li>
                    </ol>
                </nav>
            </h5>

            <div class="content-ati">
                <div class="row">
                    <div class="col-md-12" >
                        <div data-aos="fade-up" data-aos-duration="2000"> <i class="far fa-check-circle"></i> Lorem
                            ipsum dolor sit amet consectetur adipisicing elit. Repudiandae rem voluptate ad?
                            Exercitationem, tenetur delectus quae dolorem porro facilis recusandae quo illo modi quod.
                            Qui aut quas quisquam nam similique?
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="box-plus" data-aos="flip-up" data-aos-delay="800" data-aos-offset="500"
                            data-aos-duration="1600">1. Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            accusantium officia libero aspernatur adipisci error doloribus, explicabo atque perspiciatis
                            modi.</div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="box-plus" data-aos="flip-up" data-aos-delay="1200" data-aos-offset="500"
                            data-aos-duration="1600">2. Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            accusantium officia libero aspernatur adipisci error doloribus, explicabo atque perspiciatis
                            modi.</div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-plus" data-aos="flip-up" data-aos-delay="1600" data-aos-offset="500"
                            data-aos-duration="1600">3. Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            accusantium officia libero aspernatur adipisci error doloribus, explicabo atque perspiciatis
                            modi.</div>
                    </div>
                    <div class="col-md-12">
                        <div class="fade-content" data-aos="fade" data-aos-delay="2000">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit expedita libero, dolorum quam
                            quod enim distinctio perferendis quos veritatis aliquam voluptatibus quas quaerat alias
                            labore fugit modi rerum, soluta optio.
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit expedita libero, dolorum quam
                            quod enim distinctio perferendis quos veritatis aliquam voluptatibus quas quaerat alias
                            labore fugit modi rerum, soluta optio.
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit expedita libero, dolorum quam
                            quod enim distinctio perferendis quos veritatis aliquam voluptatibus quas quaerat alias
                            labore fugit modi rerum, soluta optio.
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit expedita libero, dolorum quam
                            quod enim distinctio perferendis quos veritatis aliquam voluptatibus quas quaerat alias
                            labore fugit modi rerum, soluta optio.
                        </div>
                    </div>
                </div>
            </div>






        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>