<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <style>
      /*Page 24*/
    .col-sm-12.text-toggle{
      width: 1100px;
      height: 300px;
      top: 50px;
      border: 2px solid #11538c;
    }
    a .btn_box_1{
      font-family: Helvetica;
      letter-spacing: 2px;
      text-transform: uppercase;
      text-decoration: none;
    /*  box-shadow: 0 20px 50px rgba(0,0,0,.4);*/
      list-style: none;
      text-align: center;
      padding-top: 10px;
      padding-bottom: 10px;
    /*  border-radius: 100px;*/
      /*background: #3399ff;*/
      color: #000000;
      text-decoration: none;
      font-size: 1.30em;
      font-weight: bold;
    }
    a .btn_box_2{
      font-family: Helvetica;
      letter-spacing: 2px;
      text-transform: uppercase;
      text-decoration: none;
    /*  box-shadow: 0 20px 50px rgba(0,0,0,.4);*/
      list-style: none;
      text-align: center;
      padding-top: 10px;
      padding-bottom: 10px;
      list-style: none;
      text-align: center;
    /*  border-radius: 100px;*/
      /*background: #F7CA18;*/
      color: #000000;
      text-decoration: none;
      font-size: 1.30em;
      font-weight: bold;
    }
    a .btn_box_3{
      font-family: Helvetica;
      letter-spacing: 2px;
      text-transform: uppercase;
      text-decoration: none;
    /*  box-shadow: 0 20px 50px rgba(0,0,0,.4);*/
      list-style: none;
      text-align: center;
      padding-top: 10px;
      padding-bottom: 10px;
      list-style: none;
      text-align: center;
    /*  border-radius: 100px;*/
      /*background: #ff5050;*/
      color: #000000;
      text-decoration: none;
      font-size: 1.30em;
      font-weight: bold;
    }
    .bold{
      color: black;
    }

    /*animation button*/

    a.animated-button.thar-one {
      color: #fff;
      cursor: pointer;
      display: block;
      position: relative;
      border: 2px solid #3399ff;
      transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
    }
    a.animated-button.thar-one:hover {
      color: #000 !important;
      background-color: transparent;
      text-shadow: none;
    }
    a.animated-button.thar-one:hover:before {
      bottom: 0%;
      top: auto;
      height: 100%;
    }
    a.animated-button.thar-one:before {
      display: block;
      position: absolute;
      left: 0px;
      top: 0px;
      height: 0px;
      width: 100%;
      z-index: -1;
      content: '';
      color: #000 !important;
      background: #3399ff;
      transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
    }

    a.animated-button.thar-two {
      color: #fff;
      cursor: pointer;
      display: block;
      position: relative;
      border: 2px solid #F7CA18;
      transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
    }
    a.animated-button.thar-two:hover {
      color: #000 !important;
      background-color: transparent;
      text-shadow: none;
    }
    a.animated-button.thar-two:hover:before {
      top: 0%;
      bottom: auto;
      height: 100%;
    }
    a.animated-button.thar-two:before {
      display: block;
      position: absolute;
      left: 0px;
      bottom: 0px;
      height: 0px;
      width: 100%;
      z-index: -1;
      content: '';
      color: #000 !important;
      background: #F7CA18;
      transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
    }

    a.animated-button.thar-three {
      color: #fff;
      cursor: pointer;
      display: block;
      position: relative;
      border: 2px solid #ff5050 ;
      transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
    }
    a.animated-button.thar-three:hover {
      color: #000 !important;
      background-color: transparent;
      text-shadow: none;
    }
    a.animated-button.thar-three:hover:before {
      bottom: 0%;
      top: auto;
      height: 100%;
    }
    a.animated-button.thar-three:before {
      display: block;
      position: absolute;
      left: 0px;
      top: 0px;
      height: 0px;
      width: 100%;
      z-index: -1;
      content: '';
      color: #000 !important;
      background: #ff5050;
      transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
    }
    .btn.btn-sm.animated-button.thar-one{
        padding-bottom: 0px;
        padding-top: 0px;
        padding-right: 0px;
        padding-left: 0px;
    }
    .btn.btn-sm.animated-button.thar-two{
        padding-bottom: 0px;
        padding-top: 0px;
        padding-right: 0px;
        padding-left: 0px;
    }
    .btn.btn-sm.animated-button.thar-three{
        padding-bottom: 0px;
        padding-top: 0px;
        padding-right: 0px;
        padding-left: 0px;
    }
    /*/end page 24*/
    </style>

    <title>Press Tab - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                24. Press Tab
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>
            <div class="content-ati" style="padding-top: 2.7em;">
            <div class="row">
                <div class="col-sm-4">
                    <a href="#all-pager" class="btn btn-sm animated-button thar-one" >
                        <li class="btn_box_1">Choice ATI 1</li>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="#all-pager" class="btn btn-sm animated-button thar-two">
                        <li class="btn_box_2">Choice ATI 2</li>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="#all-pager" class="btn btn-sm animated-button thar-three">
                        <li class="btn_box_3">Choice ATI 3</li>
                    </a>
                </div>
            </div>
            <div class="col-sm-12 text-toggle" id="box1" >
                <br><p><b class="bold">Test Box 1</b> : Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                    voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!</p>
                <ul>
                     <li>Test Box 1  Lorem ipsum dolor sit amet consectetur</li>
                     <li>Test Box 1  Lorem ipsum dolor sit amet consectetur</li>
                     <li>Test Box 1  Lorem ipsum dolor sit amet consectetur</li>
                </ul>
            </div> 
            <div class="col-sm-12 text-toggle" id="box2">
                <br><p><b class="bold">Test Box 2</b> : Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                    voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!</p>
            </div>
            <div class="col-sm-12 text-toggle" id="box3">
                <br><p><b class="bold">Test Box 3</b> : Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                    voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!</p>
                 <ul>
                    <li>Test Box 3  Lorem ipsum dolor sit amet consectetur</li>
                    <li>Test Box 3  Lorem ipsum dolor sit amet consectetur</li>
                    <li>Test Box 3  Lorem ipsum dolor sit amet consectetur</li>
                     <hr>
                </ul>
                 <p>Test Box 3  Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident,
                                    voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique!</p>
            </div>
    </div>    

    </div>
    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
     <script>
    $(document).ready(function(){
      $( "#box1").show( );
      $( "#box2" ).hide( );
      $( "#box3" ).hide( );

      $( ".btn_box_1" ).click(function() {
      $(" #box1").toggle( "slow" );
      $( "#box2" ).hide( "slow" );
      $( "#box3" ).hide( "slow" );
      });

      $( ".btn_box_2" ).click(function() {
      $( "#box2" ).toggle( "slow" );
      $( "#box1" ).hide( "slow" );
      $( "#box3" ).hide( "slow" );
      });

      $( ".btn_box_3" ).click(function() {
      $( "#box3" ).toggle( "slow" );
      $( "#box1" ).hide( "slow" );
      $( "#box2" ).hide( "slow" );
      });
    });
    </script>

</body>

</html>