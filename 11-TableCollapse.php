<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Table Collapse - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                11. Table Collapse
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Table Collapse</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae rem voluptate ad? Exercitationem, tenetur delectus quae dolorem porro facilis recusandae quo illo modi quod. Qui aut quas quisquam nam similique?
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae rem voluptate ad? Exercitationem, tenetur delectus quae dolorem porro facilis recusandae quo illo modi quod. Qui aut quas quisquam nam similique?
                        </div>
                        <div class="col-md-2">
                            <div class="box-table"><span class="table-11"> title ATI 01</span></div>
                            <div class="box-table-11"><span class="table-value"> value ATI 01</span></div>
                            <div class="box-table-11"><span class="table-value"> value ATI 02</span></div>
                            <div class="box-table-11"><span class="table-value"> value ATI 03</span></div>
                            <div class="box-table-11"><span class="table-value"> value ATI 04</span></div>
                        </div>
                        <div class="col-md-5">
                            <div class="box-table"><a class="table-head click-1"> Click here 01</a></div>
                            <div class="box-table-11"><span class="table-value-11 box-1 value-1"> value ATI 01</span></div>
                            <div class="box-table-11"><span class="table-value-11 box-1 value-2"> value ATI 02</span></div>
                            <div class="box-table-11"><span class="table-value-11 box-1 value-3"> value ATI 03</span></div>
                            <div class="box-table-11"><span class="table-value-11 box-1 value-4"> value ATI 04</span></div>
                        </div>
                        <div class="col-md-5">
                            <div class="box-table"><a class="table-head  click-2"> Click here 02</a></div>
                            <div class="box-table-11"><span class="table-value-11 box-2 value-5"> value ATI 01</span></div>
                            <div class="box-table-11"><span class="table-value-11 box-2 value-6"> value ATI 02</span></div>
                            <div class="box-table-11"><span class="table-value-11 box-2 value-7"> value ATI 03</span></div>
                            <div class="box-table-11"><span class="table-value-11 box-2 value-8"> value ATI 03</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'themes/template/include/footer.php'; ?>
        </div>
        <?php include 'themes/template/include/javascript.php'; ?>
        <script src="themes/template/js/jquery-1.12.4.js"></script>
<script>
    AOS.init();
</script>

<script>
        $(document).ready(function(){
      $( ".click-1" ).click(function() {
        $(".value-1").fadeIn();
        $(".value-2").fadeIn("slow");
        $(".value-3").fadeIn("slow");
        $(".value-4").fadeIn("slow");
        $(".box-1").addClass("table-value-block");
      });
      $( ".click-2" ).click(function() {
        $(".value-5").fadeIn();
        $(".value-6").fadeIn("slow");
        $(".value-7").fadeIn("slow");
        $(".value-8").fadeIn("slow");
        $(".box-2").addClass("table-value-block");
      });
    });
</script>
    </body>
</html>