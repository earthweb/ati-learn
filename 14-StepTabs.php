<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Step Tabs - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                14. Step Tabs
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Step Tabs</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        <div class="list-content">
                            <ul>
                                <br>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas eligendi soluta nesciunt enim repudiandae dolor ullam dolores incidunt aliquam ipsa molestias, unde nam impedit ratione aspernatur voluptatem nulla minima odio!</li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores accusantium cupiditate temporibus quo voluptatibus iste accusamus reiciendis incidunt saepe sequi culpa tempora ab deleniti inventore quis aliquam rerum, magnam quas!</li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores accusantium cupiditate temporibus quo voluptatibus iste accusamus reiciendis incidunt saepe sequi culpa tempora ab deleniti inventore quis aliquam rerum, magnam quas!</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <div class="arrow-steps clearfix">
                                <div class="step step-btn-1"  style="margin-left:9em; margin-top:1em;"> <span> Step 1</span> </div>
                                <div class="step step-btn-2"  style="margin-top:1em;"> <span>Step 2 </span> </div>
                                <div class="step step-btn-3 " style="margin-top:1em;"> <span> Step 3</span> </div>
                                <div class="step step-btn-4 " style="margin-top:1em;"> <span>Step 4</span> </div>
                            </div>
                            <div class="step-14 step-box-1">
                                <p><b>Step 1</b>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident ipsa eaque in doloribus tenetur modi ex
                                    eos voluptates saepe nesciunt quia, placeat molestiae eveniet reiciendis nemo pariatur nulla dolorum consectetur?
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident ipsa eaque in doloribus tenetur modi ex eos voluptates saepe nesciunt quia, placeat molestiae eveniet reiciendis nemo pariatur nulla dolorum consectetur?
                                </p>
                            </div>
                            <div class="step-14 step-box-2">
                                <p><b>Step 2</b> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident ipsa eaque in doloribus tenetur modi ex eos voluptates saepe nesciunt quia, placeat molestiae eveniet reiciendis nemo pariatur nulla dolorum consectetur?</p>
                            </div>
                            <div class="step-14 step-box-3">
                                <p><b>Step 3</b> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident ipsa eaque in doloribus tenetur modi ex eos voluptates saepe nesciunt quia, placeat molestiae eveniet reiciendis nemo pariatur nulla dolorum consectetur? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident ipsa eaque in doloribus tenetur modi ex eos voluptates saepe nesciunt quia, placeat molestiae eveniet reiciendis nemo pariatur nulla dolorum consectetur?</p>
                            </div>
                            <div class="step-14 step-box-4">
                                <p><b>Step 4</b> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident ipsa eaque in doloribus tenetur modi ex eos voluptates saepe nesciunt quia, placeat molestiae eveniet reiciendis nemo pariatur nulla dolorum consectetur?</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <?php include 'themes/template/include/footer.php'; ?>
    </div>
    <?php include 'themes/template/include/javascript.php'; ?>
<script>
    $(document).ready(function(){
        $( ".step-btn-1" ).click(function() {
        $(" .step-box-1").show( 'slow');
        $(" .step-box-2").hide( 'slow');
        $(" .step-box-3").hide( 'slow');
        $(" .step-box-4").hide( 'slow');
        $(".step-btn-1").addClass('current');
    });
    $( ".step-btn-2" ).click(function() {
        $(" .step-box-2").show( 'slow');
        $(" .step-box-1").hide( 'slow');
        $(" .step-box-3").hide( 'slow');
        $(" .step-box-4").hide( 'slow');
        $(".step-btn-2").addClass('current');
    });
    $( ".step-btn-3" ).click(function() {
        $(" .step-box-3").show( 'slow');
        $(" .step-box-2").hide( 'slow');
        $(" .step-box-1").hide( 'slow');
        $(" .step-box-4").hide( 'slow');
        $(".step-btn-3").addClass('current');
    });
    $( ".step-btn-4" ).click(function() {
        $(" .step-box-4").show( 'slow');
        $(" .step-box-1").hide( 'slow');
        $(" .step-box-2").hide( 'slow');
        $(" .step-box-3").hide( 'slow');
        $(".step-btn-4").addClass('current');
    });
});
</script>
</body>
</html>