<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title>Mindmap Box - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                8. Mindmap Box
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Mindmap Box</li>
                    </ol>
                </nav>
            </h5>

            <div class="content-ati">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mindmap-8">

                            <div class="top-mindmap">
                                <img src="themes/template/img/mindmap/2.png" data-aos="zoom-out" data-aos-easing="ease-in-sine"
                                    data-aos-delay="300" alt="">
                                <img src="themes/template/img/mindmap/3.png" data-aos="zoom-out" data-aos-easing="ease-in-sine"
                                    data-aos-delay="500" alt="">
                                <img src="themes/template/img/mindmap/4.png" data-aos="zoom-out" data-aos-easing="ease-in-sine"
                                    data-aos-delay="700" alt="">
                            </div>
                            <div class="center-mindmap"> <img src="themes/template/img/mindmap/1.png" data-aos="zoom-out"
                                    data-aos-easing="ease-in-sine" data-aos-delay="100" alt=""></div>

                            <div class="bottom-mindmap">
                                <img src="themes/template/img/mindmap/7.png" data-aos="zoom-out" data-aos-easing="ease-in-sine"
                                    data-aos-delay="900" class="" alt="">
                                <img src="themes/template/img/mindmap/6.png" data-aos="zoom-out" data-aos-easing="ease-in-sine"
                                    data-aos-delay="1100" class="" alt="">
                                <img src="themes/template/img/mindmap/5.png" data-aos="zoom-out" data-aos-easing="ease-in-sine"
                                    data-aos-delay="1300" class="" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

</body>

</html>