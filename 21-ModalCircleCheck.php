<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Modal Circle Check - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                21. Modal Circle Check
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>
            <div class="content-ati">
                <div class="row">
                    <div class="col-md-11">
                        <ul class="modal-circle-list">
                             <li class="cirlist-item">item - A <span class="circle-check"><i class="fas fa-plus-circle plus-1"> </i> <i class="fas fa-check-circle check-1"></i></span></li>
                             <li class="cirlist-item">item - B <span class="circle-check"><i class="fas fa-plus-circle plus-2"></i> <i class="fas fa-check-circle check-2"></i></span></li>
                             <li class="cirlist-item">item - C <span class="circle-check"><i class="fas fa-plus-circle plus-3"></i> <i class="fas fa-check-circle check-3"></i></span></li>
                             <li class="cirlist-item">item - D <span class="circle-check"><i class="fas fa-plus-circle plus-4"></i> <i class="fas fa-check-circle check-4"></i></span></li>
                         </ul>
                                       
                    </div>
                 </div>
            </div>
        </div>
    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script>
    $(document).ready(function(){
      $( ".plus-1" ).click(function() {
        $(" .check-1").show( 'slow');
        $(" .plus-1").hide( 'slow');
        swal("Item - A", " Lorem ipsum dolor sit amet consectetur!");
      });
      $( ".plus-2" ).click(function() {
        $(" .check-2").show( 'slow');
        $(" .plus-2").hide( 'slow');
        swal("Item - B", " Lorem ipsum dolor sit amet consectetur!");
      });
      $( ".plus-3" ).click(function() {
        $(" .check-3").show( 'slow');
        $(" .plus-3").hide( 'slow');
        swal("Item - C", " Lorem ipsum dolor sit amet consectetur!");
      });
      $( ".plus-4" ).click(function() {
        $(" .check-4").show( 'slow');
        $(" .plus-4").hide( 'slow');
        swal("Item - D", " Lorem ipsum dolor sit amet consectetur!");
      });
    
    });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>