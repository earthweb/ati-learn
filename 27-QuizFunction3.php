<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Quiz Function 3 - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>
    
    <style>
        .exams-select {
            margin-top: 0.8em;
        }

        .exams-select h6 {
            padding-bottom: 10px;
            border-bottom: 1px solid rgba(0, 0, 0, .075);
        }

        .item-select-27 {
            padding: 2em;
            background-color: rgba(0, 0, 0, .075);
            margin: 1em 0;
        }

        .form-check-input {
            width: 25px;
            height: 25px;
        }

        .item-select-27 .form-check-label {
            margin-left: 1em;
        }

        .false-check {
            display: none;
            color: #dc3545;
            font-size: 1.5em;
            position: absolute;
            top: 45px;
            left: 10px;
        }

        .true-check {
            display: none;
            color: #28a745;
            font-size: 1.5em;
            position: absolute;
            top: 45px;
            left: 10px;
        }
        .show-box{
            display: none;
        }
    </style>
</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                27. Quiz Function 3
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>
            <div class="content-ati">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="exams-select">
                                    <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit temporibus nisi
                                        voluptates quisquam! Commodi aliquam dignissimos tempora fugiat, nisi,
                                        blanditiis consectetur culpa officia cupiditate ducimus perspiciatis eum veniam,
                                        ex ratione.</h6>
                                    <h5 style="padding-top:1em;"><b>คำถาม :</b> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Et
                                        sapiente ?</h5>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="form-check item-select-27">
                                                    <input class="form-check-input " type="radio"
                                                        name="exampleRadios" id="exampleRadios1" value="option1"
                                                        checked>
                                                    <label class="form-check-label" for="exampleRadios1">
                                                    <i class="fa fa-times-circle false-check" aria-hidden="true"></i>
                                                        <b> A.</b> Lorem ipsum dolor sit amet consectetur adipisicing
                                                        elit. Quisquam exercitationem placeat id
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-check item-select-27">
                                                    <input class="form-check-input" type="radio"
                                                        name="exampleRadios" id="exampleRadios2" value="option1"
                                                        checked>
                                                    <i class="fa fa-times-circle false-check" aria-hidden="true"></i>

                                                    <label class="form-check-label" for="exampleRadios2">
                                                        <b> B.</b> Lorem ipsum dolor sit amet consectetur adipisicing
                                                        elit. Quisquam exercitationem placeat id
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-check item-select-27">
                                                    <input class="form-check-input" type="radio"
                                                        name="exampleRadios" id="exampleRadios3" value="option1"
                                                        checked>
                                                    <i class="fa fa-times-circle false-check" aria-hidden="true"></i>
                                                    <label class="form-check-label" for="exampleRadios3">
                                                        <b> C.</b> Lorem ipsum dolor sit amet consectetur adipisicing
                                                        elit. Quisquam exercitationem placeat id
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-check item-select-27">
                                                    <input class="form-check-input " type="radio" name="exampleRadios"
                                                        id="exampleRadios4" value="option1" checked>
                                                    <i class="fa fa-check-circle true-check" aria-hidden="true"></i>
                                                    <label class="form-check-label" for="exampleRadios4">
                                                        <b> D.</b> Lorem ipsum dolor sit amet consectetur adipisicing
                                                        elit. Quisquam exercitationem placeat id
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col">
                                                <button type="submit" name="submit" value="submit"
                                                    class="btn btn-primary submit-box" id="submit" onclick="changeHtml(this)"
                                                    data-toggle="modal" data-target="#popup">Submit</button>
                                                <button type="submit" name="submit" value="submit"
                                                    class="btn btn-primary show-box">Show Answer
                                                </button>
                                                <a href="#" class="btn btn-secondary disabled">Show feedback</a>
                                            </div>

                                            <div class="modal" tabindex="-1" role="dialog" id="popup">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Incorrect!</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Select "show answer" to see the correct answer(s).</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary check-submit"
                                                                data-dismiss="modal">OK GOT IT!</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
        </div>

    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>

    <script>
            $(document).ready(function() {
                $(".check-submit").click(function () {
                    $(".form-check-input").hide();
                    $(".submit-box").hide();
                    $(".show-box").show();
                    
                });
                $(".show-box").click(function () {
                    $(".true-check").show();
                    $(".false-check").show();
                    $(".false-check").show();
                });
            });
        </script>

</body>

</html>