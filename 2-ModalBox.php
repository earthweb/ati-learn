<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
  <link rel="shortcut icon" type="image/png" href="favicon.png" />

  <title>Modal Box - ATI </title>

  <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

  <div class="body ia-main">

    <?php include 'themes/template/include/header.php'; ?>

    <div class="frame-ati">

      <h5 class="title-h5">
        2. Modal Box
        <nav aria-label="breadcrumb">
          <div class="clearfix"></div>
          <ol class="breadcrumb bc-ati">
            <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Modal Box</li>
          </ol>
        </nav>
      </h5>


      <div class="content-ati">
        <div class="row">
          <div class="col-md-6">
            <div class="group-item-ati">
              <a href="#">
                <li class="ati-colum btn-box-1">
                  <p>Choice ATI #1</p>
                </li>
              </a>
              <a href="#">
                <li class="ati-colum btn-box-2">
                  <p>Choice ATI #2</p>
                </li>
              </a>
              <a href="#">
                <li class="ati-colum btn-box-3">
                  <p>Choice ATI #3</p>
                </li>
              </a>
              <a href="#">
                <li class="ati-colum btn-box-4">
                  <p>Choice ATI #4</p>
                </li>
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card-atiview" id="box-1">
              <ul>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis.</li>
              </ul>
            </div>
            <div class="card-atiview" id="box-2">
              <ul>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis.</li>
                <li>reiciendis eligendi? Vitae excepturi repudiandae quod
                  cum labore officia neque mollitia perferendis! Non obcaecati debitis natus illum tempore eveniet
                  aliquid saepe.</li>
              </ul>
            </div>
            <div class="card-atiview" id="box-3">
              <ul>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis.</li>
                <li>reiciendis eligendi? Vitae excepturi repudiandae quod
                  cum labore officia neque mollitia perferendis! Non obcaecati debitis natus illum tempore eveniet
                  aliquid saepe.</li>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis.</li>
              </ul>
            </div>
            <div class="card-atiview" id="box-4">
              <ul>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis.
                </li>
                <li>reiciendis eligendi? Vitae excepturi repudiandae quod
                  cum labore officia neque mollitia perferendis! Non obcaecati debitis natus illum tempore eveniet
                  aliquid saepe.
                </li>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis.
                </li>
                <li>reiciendis eligendi? Vitae excepturi repudiandae quod
                  cum labore officia neque mollitia perferendis! Non obcaecati debitis natus illum tempore eveniet
                  aliquid saepe.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>


    </div>

    <?php include 'themes/template/include/footer.php'; ?>

  </div>

  <?php include 'themes/template/include/javascript.php'; ?>
  <script>
    $(document).ready(function () {
      $(".btn-box-1").click(function () {
        $(" #box-1").toggle("slow");
        $("#box-2").hide("slow");
        $("#box-3").hide("slow");
        $("#box-4").hide("slow");

      });
      $(".btn-box-2").click(function () {
        $("#box-2").toggle("slow");
        $("#box-1").hide("slow");
        $("#box-3").hide("slow");
        $("#box-4").hide("slow");
      });
      $(".btn-box-3").click(function () {
        $("#box-3").toggle("slow");
        $("#box-1").hide("slow");
        $("#box-2").hide("slow");
        $("#box-4").hide("slow");
      });
      $(".btn-box-4").click(function () {
        $("#box-4").toggle("slow");
        $("#box-1").hide("slow");
        $("#box-2").hide("slow");
        $("#box-3").hide("slow");
      });
    });
  </script>

</body>

</html>