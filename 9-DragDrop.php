<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Drag Drop - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                9. Drag Drop
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Drag Drop</li>
                    </ol>
                </nav>
            </h5>

            <div class="content-ati drop">
                <div class="row">
                    <div class="col-md-4" style="padding-top:20px;">
                        <div id="draggable-1" class="draglist"><i class="fas fa-expand-arrows-alt"></i> Drag me 01</div>
                        <div id="draggable-2" class="draglist"><i class="fas fa-expand-arrows-alt"></i> Drag me 02</div>
                        <div id="draggable-3" class="draglist"><i class="fas fa-expand-arrows-alt"></i> Drag me 03</div>
                        <div id="draggable-4" class="draglist"><i class="fas fa-expand-arrows-alt"></i> Drag me 04</div>
                    </div>
                    <div class="col-md-8">
                        <div class="box-drop">
                            <div id="droppable-1" class="drop-box">
                                <p style="padding-left:15px; ">  1.Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                <div class="checkdrop"><img src="themes/template/img/icon/1.png" alt="" height="100"></div>
                                <div class="drop-area drop-1">Drop Here</div>
                            </div>
                            <div id="droppable-2" class="drop-box">
                                <p style="padding-left:15px; ">  2.Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                <div class="checkdrop"><img src="themes/template/img/icon/2.png" alt="" height="100"></div>
                                <div class="drop-area drop-2">Drop Here</div>
                            </div>
                            <div id="droppable-3" class="drop-box">
                                <p style="padding-left:15px; "> 3.Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                <div class="checkdrop"><img src="themes/template/img/icon/3.png" alt="" height="100"></div>
                                <div class="drop-area drop-3">Drop Here</div>
                            </div>
                            <div id="droppable-4" class="drop-box">
                                <p style="padding-left:15px; "> 4.Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                <div class="checkdrop"><img src="themes/template/img/icon/4.png" alt="" height="100"></div>
                                <div class="drop-area drop-4">Drop Here</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>


    <script src="themes/template/js/jquery-1.12.4.js"></script>
    <script src="themes/template/js/jquery-ui.js.js"></script>


    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        AOS.init();
    </script>
    <script>
        $(".draglist").draggable({
            containment: '.content-ati',
            revert: "invalid",
            snap: ".drop-box"
        });

        $(".drop-area").droppable({
            snap: ".drop-box",
            drop: function () {
                swal("Good job!", "You clicked the button!", "success");

            }
        });
    </script>
</body>

</html>