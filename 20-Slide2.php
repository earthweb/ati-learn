<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Slide 2 - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                20. Slide 2
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Slide 2</li>
                    </ol>
                </nav>
            </h5>

        <div class="content-ati">
            <div class="row">
                <div class="col-md-12">
                    <div class="slide">
                        <div class="group-slide  slide-1">
                            <div class="img-slide-left">
                                <img src="themes/template/img/slide-1.png" alt="">
                                <div class="box-imgslide">
                                        <a href="#" class="prev-slide "><i class="fas fa-arrow-left"></i></a>
                                        <span>
                                            1/3
                                        </span>
                                        <a href="#" class="next-slide next-1"><i class="fas fa-arrow-right"></i></a>
                                                                    
                                </div>
                             </div>
                            <div class="text-slide">
                                <h6><b>Slide 1</b></h6>
                                     Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis suscipit, ea voluptas ex maxime aut possimus veniam dolore laborum voluptate? Ut ducimus dolorum nemo quis ratione dolor architecto illo maiores!
                                     <hr>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto illo hic delectus nihil saepe laudantium voluptatem pariatur veniam libero. Quae eligendi, id quod et ea unde! Impedit ab consequuntur asperiores.
                            </div>  
                        </div>

                                <div class="group-slide slide-2">
                                <div class="img-slide-left">
                                    <img src="themes/template/img/slide-3.png" alt="">
                                    <div class="box-imgslide">
                                            <a href="#" class="prev-slide back-1"><i class="fas fa-arrow-left"></i></a>
                                            <span>
                                                 2/3
                                             </span>
                                            <a href="#" class="next-slide next-2"><i class="fas fa-arrow-right"></i></a>
                                                                
                                    </div>
                                </div>
                                 <div class="text-slide">
                                     <h6><b>Slide 2</b></h6>
                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis suscipit, ea voluptas ex maxime aut possimus veniam dolore laborum voluptate? Ut ducimus dolorum nemo quis ratione dolor architecto illo maiores!
                                        <hr>
                                                            
                                </div>  
                            </div>

                                <div class="group-slide slide-3">
                                <div class="img-slide-left">
                                    <img src="themes/template/img/slide-2.png" alt="">
                                     <div class="box-imgslide">
                                            <a href="#" class="prev-slide back-2"><i class="fas fa-arrow-left"></i></a>
                                            <span>
                                                 3/3
                                            </span>
                                            <a href="#" class="next-slide"><i class="fas fa-arrow-right"></i></a>
                                                            
                                    </div>
                                </div>
                                <div class="text-slide">
                                    <h6><b>Slide 3</b></h6>
                                         Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis suscipit, ea voluptas ex maxime aut possimus veniam dolore laborum voluptate? Ut ducimus dolorum nemo quis ratione dolor architecto illo maiores!
                                        <hr>
                                        <div class="text-center">  <img src="themes/template/img/logo-ATI png.png" alt="" style="height:100px"></div>
                                </div>  
                            </div>
                        </div>
                    </div>             
                 </div>
        </div>


    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>

    <script>
    $(document).ready(function(){
      $( ".next-1" ).click(function() {
        $(" .slide-2").show( 'slow');
        $(" .slide-1").hide( 'slow');
      });

      $( ".back-1" ).click(function() {
        $(" .slide-1").show( 'slow');
        $(" .slide-2").hide( 'slow');
      });

      $( ".next-2" ).click(function() {
        $(" .slide-3").show( 'slow');
        $(" .slide-2").hide( 'slow');
      });
      $( ".back-2" ).click(function() {
        $(" .slide-2").show( 'slow');
        $(" .slide-3").hide( 'slow');
      });
    });
    </script>

</body>

</html>