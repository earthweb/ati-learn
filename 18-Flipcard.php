<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
        <link rel="shortcut icon" type="image/png" href="favicon.png" />
        <title>Flip card - ATI </title>
        <?php include 'themes/template/include/css.php'; ?>
        <link href="themes/template/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="body ia-main">
            <?php include 'themes/template/include/header.php'; ?>
            <div class="frame-ati">
                <h5 class="title-h5">
                18. Flip card
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Flip card</li>
                    </ol>
                </nav>
                </h5>
                <div class="content-ati">
                    <div class="row">
                        
                        <div class="col-12" style="margin-top: 10px">
                            <br>
                            <h6>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia maiores dolorem maxime quae. Similique unde dolore rerum doloribus, itaque quia ad voluptate repellendus. Optio autem totam obcaecati magnam. Perspiciatis, cumque.</h6>
                            <div class="row text-center flip-box">
                                <div class="col-lg-3 col-md-4 mb-4">
                                    <div class="scene">
                                        <div class="card">
                                            <div class="card__face card__face--front">
                                                <div class="card">
                                                    <img class="card-img-top" src="themes/template/img/icon/6.png" alt="">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Flip - 1</h4>
                                                        <p class="card-text"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card__face card__face--back">
                                                <div class="contentflip">Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nostrum numquam fuga asperiores eum, </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 mb-4">
                                    <div class="scene">
                                        <div class="card1">
                                            <div class="card__face card__face--front">
                                                <div class="card">
                                                    <img class="card-img-top" src="themes/template/img/icon/2.png" alt="">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Flip - 2</h4>
                                                        <p class="card-text"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card__face card__face--back">
                                                <div class="contentflip">Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nostrum numquam fuga asperiores eum, </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 mb-4">
                                    <div class="scene">
                                        <div class="card2">
                                            <div class="card__face card__face--front">
                                                <div class="card">
                                                    <img class="card-img-top" src="themes/template/img/icon/3.png" alt="">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Flip - 3</h4>
                                                        <p class="card-text"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card__face card__face--back">
                                                <div class="contentflip"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nostrum numquam fuga asperiores eum, </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 mb-4">
                                    <div class="scene">
                                        <div class="card3">
                                            <div class="card__face card__face--front">
                                                <div class="card">
                                                    <img class="card-img-top" src="themes/template/img/icon/4.png" alt="">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Flip - 4</h4>
                                                        <p class="card-text"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card__face card__face--back">
                                                <div class="contentflip"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde nostrum numquam fuga asperiores eum, </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="list-content">
                    <ul>
                    </ul>
                </div>
            </div>
        </div>
        <?php include 'themes/template/include/footer.php'; ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>



    <script>
        var card = document.querySelector('.card');
        card.addEventListener( 'click', function() {
            card.classList.toggle('is-flipped');
        });

        var card1 = document.querySelector('.card1');
        card1.addEventListener( 'click', function() {
            card1.classList.toggle('is-flipped');
        });
        var card2 = document.querySelector('.card2');
        card2.addEventListener( 'click', function() {
            card2.classList.toggle('is-flipped');
        });
        var card3 = document.querySelector('.card3');
        card3.addEventListener( 'click', function() {
            card3.classList.toggle('is-flipped');
        });


    </script>
</body>
</html>