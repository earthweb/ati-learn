<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>Mini Tabs 2 - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

    <style>


        * {

            box-sizing: border-box;

        }
        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            text-align: center;
        }

        .box-pic {
            margin: 1em 0em 1em 3em;
        }

        .middle-bnt {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 50%;
            left: 23%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .box-pic:hover .pic-item {

            opacity: 0.3;

        }

        .box-pic:hover .middle-bnt {
            opacity: 1;
        }

        .box-content-22 {
            height: 400px;
            display: none;
            border: 2px solid #11538c;
            padding: 1.5em;
            margin-top: 2em;
        }

        .pic-item {
            width: 133px;
        }

        .box-scroll {
            width: 100%;
            /* height: 10px; */
            /* min-height: 520px; */
        }

        .text {
            font-size: 30px;
            color: white;
            cursor: pointer;
        }

        .pic-item {

            border-radius: 50%;
        }

        .item-1 {
            cursor: pointer;
            background: #09909f;
            width: 133px;
            border-radius: 50%;
        }

        .item-2 {
            cursor: pointer;
            background: #5059ab;
            width: 133px;
            border-radius: 50%;
        }

        .item-3 {
            cursor: pointer;
            background: #14a0c0;
            width: 133px;
            border-radius: 50%;
        }
      
    </style>
</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                22. Mini Tabs 2
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>
            <div class="content-ati">
                    <div class="row">

                        <div class="box-scroll">

                            <!--1-->

                            <div class="col-12 box-item">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="box-pic " data-aos="fade-left" data-aos-easing="ease-out-cubic"
                                             data-aos-duration="900">
                                            <div class="item-1"><img src="themes/template/img/progress-1.png" class="pic-item"></div>
                                            <div class="middle-bnt">
                                                <div class="text text-1"><i class="fas fa-plus"></i></div>
                                            </div>
                                        </div>
    
                                        <div class="box-pic " data-aos="fade-left" data-aos-easing="ease-out-cubic"
                                             data-aos-duration="1400">
                                            <div class="item-2"><img src="themes/template/img/progress-2.png" class="pic-item"></div>
                                            <div class="middle-bnt">
                                                <div class="text text-2"><i class="fas fa-plus"></i></div>
                                            </div>
                                        </div>
    
                                        <div class="box-pic " data-aos="fade-left" data-aos-easing="ease-out-cubic"
                                        data-aos-duration="1900">
                                        <div class="item-3"><img src="themes/template/img/progress-3.png" class="pic-item"></div>
                                        <div class="middle-bnt">
                                            <div class="text text-3"><i class="fas fa-plus"></i></div>
                                        </div>
                                        </div>
                                    </div>

                                   <div class="col-md-8">
                                        <div class=" box-content-22 content-1">
                                            <p>
                                                <b>1.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid
                                                aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque
                                                sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur
                                                magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim
                                                asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur
                                                magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim
                                                asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                orem ipsum dolor sit amet consectetur adipisicing elit. Aliquid
                                                aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque
                                                sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                
                                            </p>
                                        </div>
                                        
                                        <div class=" box-content-22 content-2">
                                            <p>
                                                <b>2.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid
                                                aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque
                                                sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur
                                                magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim
                                                asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur
                                                magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim
                                                asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                            </p>
                                        </div>
                                        <div class=" box-content-22 content-3">
                                            <p>
                                                <b>3.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid
                                                aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque
                                                sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur
                                                magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim
                                                asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid aspernatur
                                                magni quaerat inventore animi nemo sequi ad, earum a doloremque sed enim
                                                asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                orem ipsum dolor sit amet consectetur adipisicing elit. Aliquid
                                                aspernatur magni quaerat inventore animi nemo sequi ad, earum a doloremque
                                                sed enim asperiores consequatur voluptatem distinctio ipsam hic dicta iusto?
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                            </p>
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        $(document).ready(function () {
            $(".item-1").click(function () {
                $(".content-1").show('slow');
                $(".content-2").hide('slow');
                $(".content-3").hide('slow');

            });
           
            $(".item-2").click(function () {
                $(".content-2").show('slow');
                $(".content-1").hide('slow');
                $(".content-3").hide('slow');

            });
          
            $(".item-3").click(function () {
                $(".content-3").show('slow');
                // $(".text-3").hide('slow');
                $(".content-1").hide('slow');
                $(".content-2").hide('slow');
            });
          
        });
    </script>
    <script>
        AOS.init();
    </script>

</body>

</html>