<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <style>
      .swal-button-container{
          margin-left: 0px;
          margin-top: 0px;
          margin-right: 0px;
          margin-bottom: 0px;
          right: 170px;
      }
    </style>
    <style >
      .middle1 {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
      }
      .card1:hover .middle1 {
        opacity: 1;
      }
      .card1:hover .pic1 {
        opacity: 0.3;
      }

      .middle2 {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
      }
      .card2:hover .middle2 {
        opacity: 1;
      }
      .card2:hover .pic2 {
        opacity: 0.3;
      }
      .middle3 {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
      }
      .card3:hover .middle3 {
        opacity: 1;
      }
      .card3:hover .pic3 {
        opacity: 0.3;
      }

      .circle-check {
        color: #11538c;
        font-size: 4em;
        padding: 16px 32px;
      }
    </style>

    <title>Modal Check - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
               23. Modal Check
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>
            <div class="content-ati" style="padding-top: 20px;">
                            <div class=""><p style="padding-top:1.7em;">Click on each <b>Picture</b></p></div>
                                <div class="row">
                                    <div class="card-deck mt-3" >
                                      <div class="card card1" style="border:none;">
                                        <img src="themes/template/img/slide-1.png" class="card-img-top pic1" type="button" alt="...">
                                        <div class="middle1">
                                            <span class="circle-check"><i class="fas fa-plus-circle plus-1"> </i> </span>
                                        </div>
                                      </div>
                                      <div class="card card2" style="border:none;">
                                        <img src="themes/template/img/slide-2.png" class="card-img-top pic2" type="button" alt="...">
                                        <div class="middle2">
                                            <span class="circle-check"><i class="fas fa-plus-circle plus-2"> </i> </span>
                                        </div>
                                      </div>
                                      <div class="card card3" style="border:none;">
                                        <img src="themes/template/img/slide-3.png" class="card-img-top pic3" type="button" alt="...">
                                          <div class="middle3">
                                            <span class="circle-check"><i class="fas fa-plus-circle plus-3"> </i> </span>
                                        </div>
                                      </div>
                                    </div>
                                    <ul class="modal-circle-list">

                                    </ul>
                                </div>
                        </div>
  
        </div>
    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->

    <script>
    $(document).ready(function(){
       $( ".card1" ).click(function() {
        swal(" Test Alert 1 :Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique !",{button: "Ok Got it",});
      });
       $( ".card2" ).click(function() {
        swal(" Test Alert 2 :Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique !",{button: "Ok Got it"});
      });
        $( ".card3" ).click(function() {
        swal("Test Alert 3 : Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro hic fugiat possimus modi minima aliquid provident voluptatum non? Minus laudantium nesciunt totam labore atque modi suscipit nam, ex eius similique !",{button: "Ok Got it"});
      });

    });
    </script>

</body>

</html>