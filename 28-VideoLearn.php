<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Video Learn - ATI </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="themes/template/fonts/fontawesome.css"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <style>
        .video-box {
            margin-top: 0.8em;
        }

        .video-box h6 {
            border-bottom: 1px solid rgba(0, 0, 0, .075);
            padding-bottom: 10px;
        }

        .video-box video {
            margin-top: 0.5em;
            border: 5px solid #495057;
            border-radius: 3px;
        }

        .video-box .ob-video {
            margin: 0 auto;
            text-align: center;
            margin-top:1em;
        }
    </style>
    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                28. Video Learn
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Video Learn</li>
                    </ol>
                </nav>
            </h5>
            <div class="content-ati">
                <div class="row">
                    <div class="col-md-12">
                        <div class="video-box">
                            <h6>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit temporibus nisi
                                        voluptates quisquam! Commodi aliquam dignissimos tempora fugiat, nisi,
                                        blanditiis consectetur culpa officia cupiditate ducimus perspiciatis eum veniam,
                                        ex ratione.
                            </h6>
                            <div class="ob-video">
                                <video height="380" controls>
                                    <source src="themes/template/video/testfile.mp4" type="video/mp4">
                                    <source src="themes/template/video/testfile.ogg" type="video/ogg">
                                                Your browser does not support the video tag.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="http://code.jquery.com/jquery-1.11.1.js"></script>

</body>

</html>