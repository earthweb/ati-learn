<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />

    <title>Quiz Function - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
                6. Quiz Function
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quiz Function</li>
                    </ol>
                </nav>
            </h5>




            <div class="content-ati">
                <div class="row">
                    <div class="col-md-4">
                        <div class="quizuser user-ati-6">
                            <img src="themes/template/img/people/quiz.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="startquiz">
                            <div class="quizdiv">
                                <div>
                                    <h5><b>Lorem, ipsum dolor sit amet consectetur adipisicing elit</b></h5>
                                </div>
                                <p>
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est porro eos in iste,
                                    nesciunt quod perspiciatis. Harum velit assumenda officia blanditiis impedit vel,
                                    illo fuga et odit. Dignissimos, odit incidunt.
                                </p>
                            </div>
                            <div class="text-center"> <a href="#" class="btn-quiz btn btn-danger">Start Quiz</a></div>
                        </div>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <div class="result-ati text-center">
                            <h4>Result</h4>
                            <div class="score-ati">
                                <span class="title-score"><b>Your Score:</b></span> 80% (2 point)
                            </div>
                            <h5><i class="fas fa-check-circle"></i> Your Success</h5>

                            <!-- <img src="themes/template/img/people/quiz-2.png" alt=""> -->
                            <div class="text-center"><a href="" class="btn-quiz-re btn btn-danger">Retry Quiz</a></div>
                        </div>
                        <div class="board-ati" style="height: 560px;">

                            <div class="q-atitext q-num-1">
                                <h5><b>1. Lorem, ipsum dolor sit amet <u>consectetur</u> adipisicing elit</b></h5>

                                <div class="q-choose">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            <b> A.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2" value="option2">
                                        <label class="form-check-label" for="exampleRadios2">
                                            <b> B. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios3" value="option2">
                                        <label class="form-check-label" for="exampleRadios3">
                                            <b>C. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios4" value="option2">
                                        <label class="form-check-label" for="exampleRadios4">
                                            <b>D. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="q-atitext q-num-2">
                                <h5><b>2. Lorem, ipsum dolor sit amet <u>consectetur</u> adipisicing elit</b></h5>

                                <div class="q-choose">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios1-1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1-1">
                                            <b> A.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2-1" value="option2">
                                        <label class="form-check-label" for="exampleRadios2-1">
                                            <b> B. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios3-1" value="option2">
                                        <label class="form-check-label" for="exampleRadios3-1">
                                            <b>C. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios4-1" value="option2">
                                        <label class="form-check-label" for="exampleRadios4-1">
                                            <b>D. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="q-atitext q-num-3">
                                <h5><b>3. Lorem, ipsum dolor sit amet <u>consectetur</u> adipisicing elit</b></h5>

                                <div class="q-choose">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios1-2" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1-2">
                                            <b> A.</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2-2" value="option2">
                                        <label class="form-check-label" for="exampleRadios2-2">
                                            <b> B. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios3-2" value="option2">
                                        <label class="form-check-label" for="exampleRadios3-2">
                                            <b>C. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios4-2" value="option2">
                                        <label class="form-check-label" for="exampleRadios4-2">
                                            <b>D. </b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
                                            exercitationem placeat id
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <a href="#" class="back-q">Back Quiz</a>
                                <a href="#" class="next-q">Next Quiz</a>
                                <a href="#" class="back-q-3">Back Quiz</a>
                                <a href="#" class="next-q-3">Next Quiz</a>
                                <a href="#" class="confirm-q">Confirm Quiz</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script>
        $(document).ready(function () {
            $(".btn-quiz").click(function () {
                $(".title-h5").hide("slow");
                $(".quizuser ").hide("slow");
                $(".startquiz").hide("slow");
                $(".frame-ati").addClass("bg-quizopen");
                $(".board-ati").show("slow");
            });
            $(".next-q").click(function () {
                $(".q-num-1").hide("slow");
                $(".q-num-2").show("slow");
                $(".back-q").show("slow");
                $(".next-q").hide('slow');
                $(".next-q-3").show('slow');
            });
            $(".back-q").click(function () {
                $(".q-num-2").hide("slow");
                $(".q-num-1").show("slow");
                $(".back-q").hide("slow");
                $(".next-q-3").hide("slow");
                $(".next-q").show('slow');
            });
            $(".next-q-3").click(function () {
                $(".q-num-2").hide("slow");
                $(".q-num-3").show("slow");
                $(".next-q-3").hide("slow");
                $(".back-q-3").show("slow");
                $(".back-q").hide("slow");
                $(".confirm-q").show("slow");
            });
            $(".back-q-3").click(function () {
                $(".q-num-3").hide("slow");
                $(".q-num-2").show("slow");
                $(".back-q-3").hide("slow");
                $(".back-q").show("slow");
            });
            $(".confirm-q").click(function () {
                $(".board-ati").hide("slow");
                $(".result-ati").show("slow");
            });
        });
    </script>


</body>

</html>