<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" type="image/png" href="favicon.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Quiz Function 2 - ATI </title>

    <?php include 'themes/template/include/css.php'; ?>

</head>

<body>

    <div class="body ia-main">

        <?php include 'themes/template/include/header.php'; ?>

        <div class="frame-ati">

            <h5 class="title-h5">
               25. Quiz Function 2
                <nav aria-label="breadcrumb">
                    <div class="clearfix"></div>
                    <ol class="breadcrumb bc-ati">
                        <li class="breadcrumb-item"><a href="index"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Toggle Box</li>
                    </ol>
                </nav>
            </h5>
             <div class="content-ati">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="padding-top:1.7em;"><b>Select the best answer from the choices</b></p>
                                    <p>Which tool will show him the bid amount he may need to get his ad on the first page of results?</p>
                                </div>
                                <div class="col-md-6">
                                    <select class="custom-select">
                                        <option selected>- select one -</option>
                                        <option value="1">Bid simulator</option>
                                        <option value="2">Keyword Planner</option>
                                        <option value="3">First-page bid estimates</option>
                                        <option value="4">Location report</option>
                                    </select>
                                </div>
                                <div class="col-md-12"><br>
                                    <p>Which tool will show him the keyword "movie reviews" get searched and what he'd need to bid for that keyword to be competitive?</p>
                                </div>

                                <div class="col-md-6">
                                    <select class="custom-select">
                                        <option selected>- select one -</option>
                                        <option value="1">Location report</option>
                                        <option value="2">First-page bid estimates</option>
                                        <option value="3">Keyword Planner</option>
                                        <option value="4">Bid simulator</option>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group col" style="margin-top: 1.4em; margin-left:-1em;">
                                            <button type="submit" name="submit" value="submit" class="btn btn-primary" id="submit" onclick="changeHtml(this)" data-toggle="modal" data-target="#popup">submit</button>
                                            <a href="#" class="btn btn-secondary disabled">show feedback</a>
                                    </div>
                                </div>

                                <div class="modal" tabindex="-1" role="dialog" id="popup">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Incorrect!</h5>
                                            </div>
                                            <div class="modal-body">
                                                <p>Select "show answer" to see the correct answer(s).</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">OK GOT IT!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <script>
                                    function changeHtml(id) {
                                        id.innerHTML = "show answer";
                                    }
                                </script>
                            </div>
                        </div>

                    </div>
                </div>
        </div>

    </div>

    <?php include 'themes/template/include/footer.php'; ?>

    </div>

    <?php include 'themes/template/include/javascript.php'; ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.1.js"></script>

</body>

</html>